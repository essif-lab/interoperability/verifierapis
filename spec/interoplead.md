### Initial interoperability sessions definition:

# Open APIs for a Verifier's component

The goal of this interoperability session is to define a set of Open APIs for the interfaces of a Verifier's component.  

To narrow down the scope and ensure a better integration between different technology stacks, we propose to work on REST APIs.

The interfaces defined below are in line with our vision of the SSI model and the role associated to a Verifier. We'd like to include other thoughts and visions, so as to achieve a greater level of interoperability.

## Verifier's component functionalities

The Verifier's role is here circumscribed to the definition set in the Essif Lab framework: 

> "The verifier functionality is to support the data collector as it tries to acquire credentials from some other party for the purpose of negotiating a business transaction. It does so by creating presentation requests (or Presentation Definition as it is called in the draft DIF specfication for Presentation Exchange) that ask for such credentials, sending them to a holder component of another party, receiving a response to such a request (which we call a 'presentation'), verifying the presentation, i.e. checking the signature and other proofs of the veracity of both the construction of the presentation as well as its contents, thus providing the Data Collector with verified data."

From our perspective, the Verifier should at least ensure the following functions:
1. Request and receive specific information to holders in the form of Verifiable Credentials (presented in a Verifiable Presentation)
2. Verify received VCs. That includes:
    1. Verify the proofs
    2. Verify the Issuer: its authority over the credential and trust level
    3. Verify the status
    4. Verify and match the ownership
    5. Verify that all credentials can be linked to the same entity 
    6. Verify that the information received matches the information requested
3. Verify that the credentials have been handled from a secure wallet
4. Authenticate the wallet owner
5. Manage consents
6. Handle user anonimity
7. Manage selective disclosure

## APIs

The APIs that we'd like to propose as an open reference for anyone aiming to achieve interoperability, while allowing to fulfill the functionalities described above are the following:

* Non-standard APIs
    * Presentation Exchange API
    * Consent Management API
    * Query Schema Registry API
    * Query Issuers Registry API
* Standard APIs
    * DID Resolution API
    * Credential Status Query API 

The relation between these APIs would be as follows:
````md
 +------------------------------------------------------------------------------------------------------------------+
 |------------------------------------------------------------------------------------------------------------------|
 ||                 ||                 ||                 ||                 ||                 ||                 ||
 || Presentation    || Consent         || Schema Registry || Issuer Registry || DID Resolution  || Credential      ||
 || Exchange        || Management      || Query           || Query           ||                 || Status          ||
 ||                 ||                 ||                 ||                 ||                 || Query           ||
 ||                 ||                 ||                 ||                 ||                 ||                 ||
 ||                 ||                 ||                 ||                 ||                 ||                 ||
 ||       API       ||       API       ||       API       ||       API       ||       API       ||      API        ||
 +------------------------------------------------------------------------------------------------------------------+
 |   1,3,4,6,7          2.5,2.6,5,6        2.6,7             2.2                2.1,2.2,2.4,4         2.3           |
 |                                                                                                                  |
 |                                                                                                                  |
 |                                                      Verifier                                                    |
 |                                                                                                                  |
 +------------------------------------------------------------------------------------------------------------------+
````

The objective of this interoperability group is to deliver a proposal for the Non-standard APIs.

## Subgrantees

Please update this document with your name if willing to participate.

## Demonstration

_Depending on subgrantees._

## Logistics

**Kick off session:**
  
- Nov 5th, 12h CET

- Meeting link: https://meet.google.com/zui-feno-ato

**Recurrent meetings:**

- Bi-weekly. 

- Day of the week and time to be agreed upon participants