# Working Package Summary

Working Package: 

Working Package Name: 

Status:

Start date:

## Participants

> A Working Package must have at least two implementors and one integrator

Type | Name | Contact Name | Contact email
--- | --- | --- | --- 
Coordinator | TBD | TBD | TBD
Implementor | TBD | TBD | TBD
Implementor | TBD | TBD | TBD
Integrator | TBD | TBD | TBD


## Terminology

* **Decentralized Identifier (DID)** as defined in [DID-CORE](https://gataca-io.github.io/verifier-apis/#bib-did-core)
  
* **DID document** as defined in [DID-CORE](https://gataca-io.github.io/verifier-apis/#bib-did-core)

* **DID resolution** as defined in [DID-RESOLUTION](https://gataca-io.github.io/verifier-apis/#bib-did-resolution)
  
* **Revocation List 2020** as defined in [REVOCATION-LIST-2020](https://w3c-ccg.github.io/vc-status-rl-2020/)   
  
* **Verifier** as defined in [VC-DATA-MODEL](https://gataca-io.github.io/verifier-apis/#bib-vc-data-model)

* **Verifiable Credential (VC)** as defined in [VC-DATA-MODEL](https://gataca-io.github.io/verifier-apis/#bib-vc-data-model)

* **Verifiable Presentation (VP)** as defined in [VC-DATA-MODEL](https://gataca-io.github.io/verifier-apis/#bib-vc-data-model)


## Scope

TBD Depending on working group

## Standard drafts and interoperability specs to be consulted 

Refs to previous/existing work

## Participate:

* **Apply as Implementor to this WP**: Create a Merge Request on GitLab editing the ReadMe of the corresponding WP. Set the existing WP Coordinator as reviewer.

* **Apply as Integrator to this WP**: Create a Merge Request on GitLab editing the ReadMe of the corresponding WP. Set the existing WP Coordinator as reviewer.

* **File a bug or comments to the WP**:
  - Create new Issues
  - Comment on existing Issues
  - Suggest improvement by opening a Merge Requests of the repository (publicly available for reading, need a Gitlab account for writing)
  - Or else [Mail To: vui@groups.io](mailto://vui@groups.io)

* **Request Access to GitLab**, or any other issue or process if no existing access to GitLab: [Mail To: vui@groups.io](mailto://vui@groups.io)





