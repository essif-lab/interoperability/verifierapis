# VERIFIER UNIVERSAL INTERFACE (VUI)

## Introduction

**The Verifier Universal Interface (VUI) is an interoperability working group that aims at building a complete set of standard
APIs for Verifier components in SSI ecosystems**.

As different technology providers build SSI solutions, it becomes critical to ensure interoperability between these
solutions. Available standards for SSI still have important gaps, leading us to an ecosystem of full-stack providers
whose approach to interoperability is building proprietary plug-ins for each one of the other available solutions. This
approach to interoperability is not scalable.

The underlying problem is that building standards take time. That is the reason that we propose a practical and focused
approach to enable scalable interoperability in the SSI community. 

We propose to start with a specific SSI component, namely the Verifier component, and lead the definition of the minimum
set of standard APIs necessary to implement or interoperate with such module. That is, **a role-centric approach to
standardization at API level**. 

To date, 12 organisations are contributing to this initiative. 

All efforts have been pushed under the NGI EssifLab organization.

![Essif Lab Logo](assets/essiflabLogo.png)

## Standard APIs

VUI identifies a minimum set of 6 APIs to offer an end-to-end credential verification flow. 

* Presentation Exchange
* Consent Management
* Issuer resolution
* Schema resolution
* DID resolution
* Credential status resolution

The APIs specifications abide to the following normative references:
* [W3C DID Core](https://www.w3.org/TR/did-core/)
* [W3C Verifiable Credentials Data Model](https://www.w3.org/TR/vc-data-model/)

Particularly, DID resolution and Credential Status resolution APIs completely follow existing normative references:
* [W3C DID Resolution](https://w3c-ccg.github.io/did-resolution/)
* [W3C Revocation List 2020](https://w3c-ccg.github.io/vc-status-rl-2020/)   

The Presentation Exchange API was defined considering and extending existing interoperability efforts. 
* [DIF Presentation Exchange](https://identity.foundation/presentation-exchange/)

The Issuer Resolution API was defined to allow independence from specific Trust Frameworks. To date Issuer Resolution API is compatible with both EBSI and GATACA's Trust Frameworks.

The scope of the ESSIF-Lab program focused on the definition and implementation of 3 APIs: 
* Presentation Exchange
* Consent Management
* Issuer resolution

Further work of VUI will involve the definition of the Schema resolution, the only API without a definition proposal or an exiting normative reference.

## VUI deliveries

### VUI API specifications in ReSpec: 
- [VUI API Specs](https://gataca-io.github.io/verifier-apis/)

The Draft is being openly maintained on a public [github repository](https://github.com/gataca-io/verifier-apis)

### Swagger documentation:
- [Presentation Exchange + Data Agreements](https://gataca-io.github.io/vui-core/index.html)
- [Issuer Resolution](https://gataca-io.github.io/vui-core/issuerResolution.html)

### VUI Core library
The [vui-core library](https://github.com/gataca-io/verifier-apis) is an open-source library made in Golang.

It provides support to any verifier that wants to integrate the validations of a full VUI Presentation Exchange.

## Participating in VUI

### Working Group (WG) Roles

VUI members may have one or more of the following roles:

* **Implementor** - entities or individuals providing a service that uses one API. An implementor is responsible for:
  * defining an API proposal
  * defining a set of interoperability tests
  * delivering a service endpoint 

* **Integrator** - entities or individuals that want to integrate a service that uses one API. An Integrator is responsible for:
  * integrating the service endpoints in their technology stack
  * executing tests 

* **Coordinator** - entities or individuals that coordinate VUI as a whole and ensure alignment between different Working Packages. GATACA is the current coordinator for VUI.


### List of existing Working Packages (WP)

Each API is managed via a different Working Package. 

#### Working Package 1 - Presentation Exchange
* Implementor: GATACA
* Implementor: Sphereon

[Presentation Exchange Repo](https://gitlab.grnet.gr/essif-lab/interoperability/WP-VUI-PresentationExchange)

#### Working Package 2 - Issuer Resolution
* Implementor: GATACA
* Implementor: UBICUA

[Issuer Resolution Repo](https://gitlab.grnet.gr/essif-lab/interoperability/WP-VUI-IssuerResolution)

#### Working Package 2 - Consent/Data Agreement
* Implementor: GATACA
* Implementor: iGrant.io

[Data Agreement Repo](https://github.com/decentralised-dataexchange/automated-data-agreements)

### Becoming a new participant
* **Subscribe to mailing list**: [Mail To: vui+subscribe@groups.io](mailto://vui+subscribe@groups.io)

* **Send an email to VUI participants**: [Mail To: vui@groups.io](mailto://vui@groups.io)

* **Join our bi-weekly meetings**: 
  * When: Thursdays 12:30pm CET. Next meeting April 29th, 2021.
  * Where: https://meet.google.com/cdd-ojiv-bnj.
  * By Phone: +34 960 46 25 94 PIN: 332319501#. View more phone numbers: https://tel.meet/cdd-ojiv-bnj?pin=1972016928798&hs=7


* **Create a new WP**: Open a new Issue for discussion on the GitLab Repository with following information:
  * WP Coordinator: single individual responsible for managing external communications 
  * WP Team: entities and/or individuals already interested in joining the WP as implementors or integrators.
  * WP Scope: ambition and rationale to create a new WP. 
  * Suggested scope and features to be implemented in a service endpoint
  * Existing interop efforts to be consulted
  * (Set the existing WP Technical Committee as reviewers)
  

* **Apply as Implementor to an existing WP**: Create a Merge Request on GitLab editing the ReadMe of the corresponding WP. Set the existing WP Coordinator as reviewers.

* **Apply as Integrator to an existing WP**: Create a Merge Request on GitLab editing the ReadMe of the corresponding WP. Set the existing WP Coordinator as reviewers.

* **File a bug or comments**:
  - Create new Issues
  - Comment on existing Issues
  - Suggest improvement by opening a Merge Requests of the repository (publicly available for reading, need a Gitlab account for writing)
  - Or else [Mail To: vui@groups.io](mailto://vui@groups.io)


* **Request Access to GitLab**, or any other issue or process if no existing access to GitLab: [Mail To: vui+owners@groups.io](mailto://vui+owners@groups.io)

### Resources for participants

[Participants profile](https://docs.google.com/spreadsheets/d/1dBDSh3n_8ryfekkW5nzu9bEXMLjXvT3QgZoP0bBnzWQ/edit#gid=0)

[Meeting minutes](https://docs.google.com/document/d/1pCt8aRSQWq3IFrN3_VjG4OAcq_RqgrkEOThDX09VZW8/edit#)



